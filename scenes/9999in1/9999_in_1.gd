extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_intro_finished():
	# Remove the current level
	get_tree().change_scene_to_file("res://scenes/pong_game/main.tscn")
